import logo from './logo.svg';
import './App.css';
import DetailPage from './page/DetailPage';


function App() {
  return (
    <div className="App">
      <DetailPage />
    </div>
  );
}

export default App;
