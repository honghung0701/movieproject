import axios from "axios"
import { userLocalService } from "./localService"

const TOKEN_CYBERSOFT =
"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJOb2RlanMgMjkiLCJIZXRIYW5TdHJpbmciOiIxMi8wOS8yMDIzIiwiSGV0SGFuVGltZSI6IjE2OTQ0NzY4MDAwMDAiLCJuYmYiOjE2Nzc2MDM2MDAsImV4cCI6MTY5NDYyNDQwMH0._Rqf64wjEFiYvyTO1ebVw_Q5phbKYLalDcLoDn-rv0U"
export const https = axios.create({
    baseURL: "https://movienew.cybersoft.edu.vn",
    headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "bearer" + userLocalService.get()?.accessToken,
    }
})